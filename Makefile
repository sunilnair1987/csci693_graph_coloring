#Setting the compiler
CC = g++

#setting compiler flags
CFLAGS = -c -g -Wall -std=c++11

all: graph

graph: main.o Graph.o
	$(CC) main.o Graph.o -o graph

main.o: main.cpp
	$(CC) $(CFLAGS) main.cpp

Graph.o: Graph.cpp Graph.h
	$(CC) $(CFLAGS) Graph.cpp

clean:
	rm *.o graph
