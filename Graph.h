#include <vector>
#include <map>
#include <string>

using namespace std;

class Graph
{
	private:
	int kColor; //chromatic number of graph
	class Vertex
	{
		public:
		string name;
		int color;
		char visited;
		string parent;
		int distance;
		bool critical;
		vector<string> *neighbors;

		Vertex(string name);
		~Vertex();
	};

	map<string , Vertex*> vertices;
	void dfsVisit(string uName);
	void color(string uName);

	public:
	Graph();
	~Graph();
	void addVertex(string name);
	void addEdge(string u, string v);
	//void greedyColoring();
	void dfsColoring(string source);
	void LDOColoring();
	void printGraph();
	int getKColor();
	void BFS(string source);
	void countKempeChains();
	void printKempeChain(vector<string> end_vertex_list);
	map<int, vector<int>> getColorSet();
	void modifiedBFS(vector<string> vertex_list, int color1, int color2);
	vector<string> getColoredVertexList(int color1, int color2);
	int criticalVerticies();
};
