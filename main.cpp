/*
	This file contains the main() function.
	The entry point of program.

	Author: Sunil Nair
*/
#include "Graph.h"
#include <iostream>
#include <sstream>

using namespace std;

int main(int argc, char *argv[])
{
	string line;
	Graph g;

	//reading first line of input std/file
	//first line of file contains all the vertices
	//of graph
	getline(cin, line);


	//the vertices are ' ' separated
	stringstream ss(line);
	string vertex;
	while(getline(ss, vertex, ' '))
	{
		//creating vertices of graph
		g.addVertex(vertex);
	}

	//subsequent lines contains the
	//edges of graph

	//Adding edges of graph
	while(getline(cin, line))
	{
		stringstream ss(line);
		string u;
		string v;
		getline(ss, u, ' ');
		getline(ss, v, ' ');

		g.addEdge(u, v);
	}

	//coloring graph
	cout << "------------------------------------------" << endl;
	cout << "LDO Coloring" << endl;
	cout << "------------------------------------------" << endl;
	g.LDOColoring();
	g.printGraph();
	cout << "No. of Critical vertices: " << g.criticalVerticies() << endl;

	cout << "------------------------------------------" << endl;
	cout << "Kempe Chains" << endl;
	cout << "------------------------------------------" << endl;
	g.countKempeChains();

	cout << "------------------------------------------" << endl;
	cout << "DFS Coloring" << endl;
	cout << "------------------------------------------" << endl;
	g.dfsColoring(argv[1]);
	g.printGraph();
	cout << "No. of Critical vertices: " << g.criticalVerticies() << endl;

	cout << "------------------------------------------" << endl;
	cout << "Kempe Chains" << endl;
	cout << "------------------------------------------" << endl;
	g.countKempeChains();

	return 0;
}
