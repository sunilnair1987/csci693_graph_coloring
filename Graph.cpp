/*
	This file contains the definition of
	Graph class.

	Author: Sunil Nair
*/
#include "Graph.h"
#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <string.h>
#include <algorithm>
#include <stdexcept>
#include <queue>

using namespace std;

//default constructor
Graph :: Graph()
{
	this->kColor = 0;
}

//destructor
Graph :: ~Graph()
{

}

//constructor for inner class Vertex
Graph :: Vertex :: Vertex(string name)
{
	this->name = name;
	this->color = -1;
	this->visited = 'W';
	this->parent = "NIL";
	this->distance = -1;
	this->neighbors = new vector<string>();
}

//destructor for inner class Vertex
Graph :: Vertex :: ~Vertex()
{

}

/*
	Function to add Vertex to graph
	input: name of vertex - string
*/
void Graph :: addVertex(string name)
{
	//creating object of vertex class
	Vertex *v = new Vertex(name);
	//adding the object to map which
	//hold all the vertices
	vertices.insert(pair<string, Vertex*>(name, v));
}

/*
	Function to add edges of graph. We add edge for both
	the vertices i.e. 'u -> v' and 'v -> u', since the
	graph is undirected graph.

	input 1: first vertex - string
	input 2: second vertex - string
*/
void Graph :: addEdge(string u, string v)
{
	//getting adjacency list of both the vertex
	Vertex *vertex_u = vertices.at(u);
	Vertex *vertex_v = vertices.at(v);

	/*
		adding adjacent nodes
	*/
	//adding v to the adjacency list of u
	vector<string> *u_neighbors = vertex_u->neighbors;
	/*
		Checking if 'v' exists in the adjacency list of u.
		if not then add to adjacency list of u.
	*/
	if(find(u_neighbors->begin(), u_neighbors->end(), v) != u_neighbors->end())
	{
		//if found do nothing
	}
	else
	{
		u_neighbors->push_back(v);
	}

	//adding u to the adjacency list of v
	vector<string> *v_neighbors = vertex_v->neighbors;
	/*
		Checking if 'u' exists in the adjacency list of v.
		if not then add to adjacency list of v.
	*/
	if(find(v_neighbors->begin(), v_neighbors->end(), u) != v_neighbors->end())
	{
			//if found do nothing
	}
	else
	{
		v_neighbors->push_back(u);
	}
}

/*
	Functioin to print the graph - vertices along with
	it adjacency list
	input - none
	output - none
*/
void Graph :: printGraph()
{
	//Iterator for accessing the map of vertices
	map<string, Vertex*> :: iterator it;
	for(it = vertices.begin(); it !=	vertices.end(); ++it)
	{
		//accessing the key - name of vertex
		string v_name = it->first;
		//accessing the value - object of vertex
		Vertex *v = it->second;

		cout << v->name << ":" << v->color << " ";
	}
	cout << endl;
	cout << "Number of colors used: " << getKColor() << endl;
}

/*
	Function to color the graph
	input : none
	output : none
*/
void Graph :: dfsColoring(string source)
{
	//setting all vertices to white and parent to NIL
	map<string, Vertex*> :: iterator it;
	for(it = vertices.begin(); it != vertices.end(); ++it)
	{
		Vertex *u = it->second;
		u->visited = 'W';
		u->parent = "NIL";
		u->color = -1;
	}

	//starting the DFS from the specified vertex
	Vertex *s = vertices.at(source);
	s->color = 0;
	dfsVisit(source);

	//visited all the vertices which are not visited
	for(it = vertices.begin(); it != vertices.end(); ++it)
	{
		Vertex *u = it->second;
		if(u->visited == 'W')
		{
			color(u->name);
			dfsVisit(u->name);
		}
	}
}

/*
	Private function DFSvisit
*/
void Graph :: dfsVisit(string source)
{
	//marking the source verex as visited
	Vertex *u = vertices.at(source);
	u->visited = 'G';

	//creating a temporary array that stores the

	vector<string> *adj_u = u->neighbors;
	vector<string> :: iterator it;
	//cout << u->name << endl;
	for(it = adj_u->begin(); it != adj_u->end(); ++it)
	{
		Vertex *v = vertices.at(*it);
		if(v->visited == 'W')
		{
			v->parent = u->name;
			color(v->name);
			dfsVisit(v->name);
		}
	}
	u->visited = 'B';
}

/*
	Function to color vertices
*/
void Graph :: color(string source)
{
	int size = vertices.size();
	bool available[size];
	for(int i = 0;i < size; i++)
	{
		available[i] = false;
	}

	Vertex *u = vertices.at(source);
	vector<string> *adj_u = u->neighbors;
	vector<string> :: iterator it;
	for(it = adj_u->begin(); it != adj_u->end(); it++)
	{
		Vertex *v = vertices.at(*it);
		if(v->color != -1)
		{
			available[(v->color)] = true;
		}
	}

	int cr;
	for(cr = 0; cr < size; cr++)
	{
		if(available[cr] == false)
		{
			break;
		}
	}
	u->color = cr;
}

/*
	Function to get the no of colors
	used to color the graph
*/
int Graph :: getKColor()
{
	//Temp variable to number of colors
	int k = 0;
	//getting the iterator
	map<string, Vertex*> :: iterator it;
	for(it = vertices.begin(); it != vertices.end(); ++it)
	{
		Vertex *v = it->second;
		if(k < v->color)
		{
			k = v->color;
		}
	}

	//since we are numbering colors from 0
	//actual count of color will n + 1
	kColor = k + 1;

	return kColor;
}

/*
	A function that implements the
	BFS graph traversal algorithm.
*/
void Graph :: BFS(string source)
{
	//setting all vertices to white and parent to NIL
	map<string, Vertex*> :: iterator it;
	for(it = vertices.begin(); it != vertices.end(); ++it)
	{
		Vertex *u = it->second;
		u->visited = 'W';
		u->parent = "NIL";
	}

	Vertex *s = NULL;

	try
	{
		//at() function throws out_of_range exception
		//if given key does not match any element in map
		s = vertices.at(source);
	}
	catch(out_of_range &oor)
	{
		cerr << "ERROR: Vertex" << source << "does not exist." << endl;
		exit(1);
	}

	//setting source values
	s->visited = 'G';
	s->distance = 0;
	s->parent = "NIL";
	s->color = 0;

	//creating queue to store the vertices as they are discovered
	queue<string> Q;

	//pushing source onto queue
	Q.push(s->name);

	//loop for traversing
	while(Q.size() != 0)
	{
		//accessing the vertex from queue
		string u_name = Q.front();
		cout << u_name << " ";
		Q.pop();	//dequeue from queue

		//going through adjacency list of u and
		//visiting each node
		Vertex *u = vertices.at(u_name);
		vector<string> *adjacencyList = u->neighbors;
		for(int i = 0; i < (int)adjacencyList->size(); i++)
		{
			string v_name = (*adjacencyList)[i];
			Vertex *v = vertices.at(v_name);

			//if not visited then
			if(v->visited == 'W')
			{
				v->visited = 'G';
				v->distance = u->distance + 1;
				v->parent = u->name;
				Q.push(v->name);
			}
		}
	}
	cout << endl;
}

/*
	Function that return a map contaning
	all possible color combination using 2 colors
	for given k colors
*/
map<int, vector<int>> Graph :: getColorSet()
{
	//getting number of colors used to color the graph
	int k = getKColor();
	//a map to store all the possible
	//chains for a given number of color
	map<int, vector<int>> color_set;

	//populating the map with colors
	for(int i = 0; i < k; i++)
	{
		/*
			Suppose K colors are used to color the graph.
			So color 1 will make pair with next K-1 colors.
			Color 2 will with next K-2. Color 3 with K-3.
			A chain is comprised of only 2 colors.
		*/
		vector<int> color_two;
		for(int j = (i + 1); j <= (k - 1); j++)
		{
			color_two.push_back(j);
		}
		//inserting into map
		color_set.insert(pair<int, vector<int> >(i, color_two));
	}

	return color_set;
}

/*
	A Function to count the kempe chains
	present in given graph
*/
void Graph :: countKempeChains()
{
	//Getting all possible color combination
	//using 2 colors for a given number of color
	map<int, vector<int>> color_set = getColorSet();

	//reading color combinations
	map<int, vector<int>> :: iterator i;
	for(i = color_set.begin(); i != color_set.end(); ++i)
	{
		//getting first color
		int color1 = i->first;
		//iterating vector for other colors
		vector<int> colors = i->second;
		vector<int> :: iterator y;
		vector<string> end_vertices;
		for(y = colors.begin(); y != colors.end(); ++y)
		{
			int color2 = *y;
			cout << "COLOR: " << color1 << " and " << color2 <<  endl;
			vector<string> vertexList = getColoredVertexList(color1, color2);
			modifiedBFS(vertexList, color1, color2);
			//printKempeChain(end_vertices);
			cout << "**********************************" << endl;
		}
	}
}

/*
	Function to print the kempe chains
*/
void Graph::printKempeChain(vector<string> end_vertex_list)
{
	for(unsigned int i = 0; i < end_vertex_list.size(); i++)
	{
		string end_vertex = end_vertex_list[i];
		Vertex *e = vertices.at(end_vertex);
		string parent = e->parent;
		while(parent.compare("NIL") != 0)
		{
			cout << e->name << " ";
			e = vertices.at(e->parent);
			parent = e->parent;
		}
		cout << e->name << endl;
	}
}

/*
	Function to get the list of all vertices int graph with
	color mentioned in parameters.
*/
vector<string> Graph :: getColoredVertexList(int color1, int color2)
{
	vector<string> vertex_list;
	//iterating through map of vertices
	map<string, Vertex*> :: iterator it;
	for(it = vertices.begin(); it != vertices.end(); ++it)
	{
		Vertex *v = it->second;
		if((v->color == color1) || (v->color == color2))
		{
			vertex_list.push_back(v->name);
		}
	}

	return vertex_list;
}

/*
	Function that discovers the kempe chain
	in graph
*/
void Graph :: modifiedBFS(vector<string> vertex_list, int color1, int color2)
{
	map<int, int> chain_numbers;
	if(vertex_list.size() == 0)
	{
		return;
	}

	//initializing all the vertices in the list
	for(unsigned int i = 0; i < vertex_list.size(); i++)
	{
		Vertex *v = vertices.at(vertex_list[i]);
		v->visited = 'W';
		v->distance = -1;
		v->parent = "NIL";
	}

	for(unsigned int i = 0;i < vertex_list.size(); i++)
	{
		bool print_u = true;
		vector<string> chain;
		Vertex *s = vertices.at(vertex_list[i]);

		if(s->visited == 'W')
		{
			s->visited = 'G';
			s->distance = 0;
			s->parent = "NIL";
			if(s->color != color1)
			{
				color2 = color1;
				color1 = s->color;
			}

			queue<string> Q;

			Q.push(s->name);

			while(Q.size() != 0)
			{
				string u_name = Q.front();
				Q.pop();

				Vertex *u = vertices.at(u_name);
				if(color1 != u->color)
				{
					color2 = color1;
					color1 = u->color;
				}
				vector<string> *adj_list = u->neighbors;
				for(unsigned int j = 0; j < adj_list->size(); j++)
				{
					string v_name = (*adj_list)[j];
					Vertex *v = vertices.at(v_name);
					if((v->visited == 'W') && (v->color == color2))
					{
						if(print_u)
						{
							//cout << u->name << " ";
							chain.push_back(u->name);
							print_u = false;
						}
						//cout << v->name << " ";
						chain.push_back(v->name);
						v->visited = 'G';
						v->parent = u_name;
						Q.push(v->name);
					}
				}
			}
			if(!print_u)
			{
				int length = chain.size() - 1;
				map<int, int> :: iterator it;
				it = chain_numbers.find(length);
				if(it != chain_numbers.end())
				{
					int current_count = chain_numbers.at(length);
					chain_numbers[length] = current_count + 1;
				}
				else
				{
					chain_numbers.insert(make_pair(length, 1));
				}
				for(unsigned int k = 0; k < chain.size(); k++)
				{
					Vertex *v = vertices.at(chain[k]);
					if(v->critical == true)
					{
						cout << v->name << "' ";
					}
					else
					{
						cout << v->name << " ";
					}
				}
				cout << endl;
			}
		}
	}
	map<int, int> :: iterator i;
	for(i = chain_numbers.begin(); i != chain_numbers.end(); ++i)
	{
		cout << i->first << " : " << i->second << endl;
	}
}

/*
	Function to get the number of critcal vertices in
	graph and mark them
*/
int Graph :: criticalVerticies()
{
	int cArray[kColor];
	int count = 0;
	map<string, Vertex*> :: iterator it;
	for(it = vertices.begin(); it != vertices.end(); ++it)
	{
		Vertex *u = it->second;
		vector<string> *uAdj = u->neighbors;
		memset(cArray, 0, sizeof(cArray));
		cArray[u->color] = 1;
		for(unsigned int i = 0; i < uAdj->size(); i++)
		{
			string vName = (*uAdj)[i];
			Vertex *v = vertices.at(vName);
			cArray[v->color] = 1;
		}
		for(int i = 0; i < kColor; i++)
		{
			if(cArray[i] == 0)
			{
				u->critical = false;
				break;
			}
			u->critical = true;
		}
		if(u->critical == true)
		{
			//cout << u->name << endl;
			count++;
		}
	}

	return count;
}

/*
	Function to color graph using
	Largest degree ordering
*/
void Graph :: LDOColoring()
{
	map<string, int> neighborLength;

	map<string, Vertex*> :: iterator it;
	for(it = vertices.begin(); it != vertices.end(); ++it)
	{
		Vertex *v = it->second;
		vector<string> *vAdj = v->neighbors;

		neighborLength.insert(make_pair(it->first, vAdj->size()));
	}

	while(neighborLength.size() > 0)
	{
		int maxLength = 0;
		string vertex = "";
		map<string, int> :: iterator i;
		for(i = neighborLength.begin(); i != neighborLength.end(); ++i)
		{
			if(maxLength < i->second)
			{
				vertex = i->first;
				maxLength = i->second;
			}
		}

		i = neighborLength.find(vertex);
		neighborLength.erase(vertex);

		color(vertex);
	}
}
